import React from 'react';
import IconHome from '../asset/icon/home.svg';
import IconUser from '../asset/icon/user.svg';
import IconMenu from '../asset/icon/menu.svg';

export const HomeIcon = ({className}) => {
  return <img src={IconHome} className={className} alt="icon"/>
}
export const UserIcon = ({className}) => {
  return <img src={IconUser} className={className} alt="icon"/>
}

export const MenuIcon = ({className}) => {
  return <img src={IconMenu} className={className} alt="icon"/>
}