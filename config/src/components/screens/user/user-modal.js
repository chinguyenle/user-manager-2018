import React, {Fragment} from 'react';
import Modal from '@material-ui/core/Modal';
import { isNull } from 'util';

import './user-modal.scss'

export default class UserModal extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      open: this.props.open ? this.props.open : false,
    };
    console.log('state', this.state)
  }

  handleOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };
  componentWillReceiveProps(nextProps){
    console.log('nextProps', nextProps);
    this.setState({
      open: nextProps.open
    })
  }

  render() {
    const { user } = this.props;
    console.log('modal', this.props);
    console.log('open state', this.state)

    return (
      <Modal
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        open={this.state.open}
        onClose={this.handleClose}
      >
        <div className="user-info">
          <div className="title">User Information</div>
          {!isNull(user) ?(
          <Fragment>
          <div className="avatar"><img src={user.picture.large} alt="Avatar"/></div>
          <div className="detail">
            <div className="text">name: <span>{user.name.first + " " +user.name.last}</span> </div>
            <div className="text">address: <span>{user.location.street + ", " +user.location.city+", "+user.location.state}</span></div>
            <div className="text">gender: <span>{user.gender}</span></div>
            <div className="text">email: <span>{user.email}</span></div>
            <div className="text">phone: <span>{user.phone}</span></div>
            <div className="text">national: <span>{user.nat}</span></div>
          </div>
          </Fragment>):<div>
            ...loading
          </div>}
        </div>
      </Modal>
    );
  }
}
