import React from 'react';
import {Link} from 'react-router-dom'
import "./slidebar.scss"
import {HomeIcon, UserIcon} from '../../common/icon'

export const SlideBar = () =>
{
  return(
    <div className="slidebar">
      <div className="website-name">
      <span className="text">
        User Manager
        </span>
      </div>
      <ul>
        <li>
          <Link to="/">
            <HomeIcon className="icon"/>
            <span>
              Home
            </span>
          </Link>
        </li>
        <li>
          <Link to="/user">
           <UserIcon className="icon"/>
           <span>
              User
           </span>
          </Link>
        </li>
      </ul>
    </div>
  
  )
}