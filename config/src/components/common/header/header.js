import React from 'react';

import './header.scss'

export default class Header extends React.Component {
  state = {}
  render() {
    return (
      <div className="header">
        <div className="content">
          <input className="input-search" type="text" placeholder="Search.." />
          <div className="user">
            <img id="avatar" src={require('../../../asset/avatar.jpg')} alt="avatar" />
            <span className="user-name">Jackey
              </span>
            <img id="icon-down" src={require('../../../asset/icon/icon-down.png')} alt="icon-down" />

          </div>
        </div>
      </div>
    );
  }
}