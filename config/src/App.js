import React, { Component } from 'react';
import Main from './components/main'
import { SlideBar } from '../src/components/slidebar/slidebar';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <SlideBar />
        <Main />
      </div>
    );
  }
}

export default App;
