import React from 'react'
import { Route, Switch} from 'react-router-dom'
import Home from '../components/screens/home/home';
import User from '../components/screens/user/user';

export default class Routes extends React.Component {
  render(){
    console.log('route')
    return(
        <Switch>
            <Route exact path='/' component={Home}/>
            <Route path='/user' component={User}/>
        </Switch>
    );
  }
}