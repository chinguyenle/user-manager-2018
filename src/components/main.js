import React from 'react';

import Header from './common/header/header';
import Routes from '../route/routes'
import { Provider} from '../context/user-context'
import "./main.scss"

export default class Main extends React.Component {
    constructor(){
      super();
      this.state={
        users:{}
      }
    }
    componentDidMount(){
      fetch('https://randomuser.me/api/?results=100')
      .then (results=>{
        return results.json();
      }).then(data=>{
        this.setState({
          users:data.results
        })
      })
    }

    render() {
        return (
          <Provider value={this.state.users}>
            <div className="container">
                <Header />
                <div className="content-layout">
                  <div className="content">
                   <Routes/>
                  </div>
                </div>
            </div>
          </Provider>
        );
    }
}