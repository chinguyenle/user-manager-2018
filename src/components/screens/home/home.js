import React, { Fragment } from 'react';
import { Pie, Bar } from 'react-chartjs';
import { Consumer } from '../../../context/user-context'

export default class Home extends React.Component {

  drawChar(users) {
    // return (this.drawPieChart(users), this.drawBarChar(users))
    let pieChart = this.drawPieChart(users);
    let barChart = this.drawBarChar(users);
    return (
      <Fragment>
        {pieChart}
        {barChart}
      </Fragment>
    )

  }
  drawPieChart(users) {
    let data = [
      {
        value: users.filter(user => user.gender === "male").length,
        color: "#F7464A",
        highlight: "#FF5A5E",
        label: "Male"
      },
      {
        value: users.filter(user => user.gender === "female").length,
        color: "#46BFBD",
        highlight: "#5AD3D1",
        label: "Female"
      },
    ]

    return (
      <div>
        <Pie data={data} width="600" height="250" />
        <h1>Gender chart</h1>
      </div>
    )
  }
  drawBarChar(users) {
    let userNationals = []
    let numberOfNat = []
    users.map(user => {
      userNationals.push(user.nat);
    })
    console.log('nats', userNationals)
    let nationals = Array.from(new Set(userNationals));
    console.log('nationls', nationals)
    console.log('user', users);
    nationals.forEach(national => {
      let couterNational = 0;
      for (let i = 0; i < userNationals.length; i++) {
        if (userNationals[i] === national) {
          couterNational++;
        }
      }
      numberOfNat.push(couterNational);
    }
    )
    let data = {
      labels: nationals,
      datasets: [
        {
          label: "My Second dataset",
          fillColor: "rgba(151,187,2,0.5)",
          strokeColor: "rgba(151,187,2,0.8)",
          highlightFill: "rgba(151,187,2,0.75)",
          highlightStroke: "rgba(151,187,205,1)",
          data: numberOfNat
        }
      ]
    }

    return (
      <div>
        <Bar data={data} width="600" height="250" />
        <h1>National chart</h1>
      </div>
    )
  }
  render() {
    return (
      <div >
        <Consumer>
          {users => {
            console.log(users)
            if (users.length > 0)
              return this.drawChar(users)
            return (
              <span>no data is display</span>
            )
          }}
        </Consumer>
      </div>
    )
  }
}