import React from 'react'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import {Consumer} from "../../../context/user-context"

import "./user.scss"
import UserModal from './user-modal';

export default class User extends React.Component {

  constructor() {
    super();
    this.state={
      openModal: false,
      userFocus:null
    }
  }
  
  componentDidMount() {
    
  }
  showUserDetail=(user)=>{
    console.log('user', user)
    console.log('open', this.state.openModal)
    this.setState({
      openModal:true,
      userFocus:user
    })
  }

  render() {
    return (
      <div>
      <UserModal open ={this.state.openModal} user={this.state.userFocus}/>
        <h1>User</h1>
        <Paper className="root">
          <Table className="table">
            <TableHead>
              <TableRow>
                <TableCell>id</TableCell>
                <TableCell >Avatar</TableCell>
                <TableCell >Name</TableCell>
                <TableCell >Email</TableCell>
                <TableCell >Phone</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              <Consumer>
                {users=>{
                  console.log('users', users)
                  if(users.length>0)
                   return users.map((user, index) => 
                      (
                        <TableRow key={index} onClick={()=>this.showUserDetail(user)}>
                          <TableCell component="th" scope="row">
                            {/* {user.id.name+" "+user.id.value} */}
                            {index+1}
                          </TableCell>
                          <TableCell ><img src={user.picture.medium} alt="Avatar"/></TableCell>
                          <TableCell >{user.name.first + " " +user.name.last}</TableCell>
                          <TableCell >{user.email}</TableCell>
                          <TableCell >{user.phone}</TableCell>
                        </TableRow>
                      )
                    )
                return( <span>'no data is display'</span>)
                  }
                }
              </Consumer>
            </TableBody>
          </Table>
        </Paper>
      </div>
    )
  }
}